BEGIN {
    print "digraph G {";
    print "rankdir=LR;";
    print "node [shape=box];";
    first=1   
}
   
   #esta funcion awk corresponde al gen_graph entregado por el profesor, no ha sido modificado por falta de
   #tiempo invertido en idearlo.
   
{


    if (first == 1) {
        print "node [shape=box];";    
        print $3$4$5 " [label = \""$3"\"];"
        first=0
    }
    
    if (serieresiduo != $5){
        print "node [shape=box];";    
        print $3$4$5 " [label = \""$3"\"];"
        if (length(old) != 0){
            if (cadena == $4){
                print old " -> " $3$4$5" ;"
            }
            
        }
        
    }
    

    old=($3$4$5)
    serieresiduo=$5
    cadena=$4
    
    if (serieresiduo == $5){
        print "node [shape=circle];";    
        atom = $1$2 
        print atom " [label = \""$1"\"];"
        if (length(old) != 0){
            print old " -> " atom ";"
        } 
    
    }
   
    
    # if (NR == 1000){
    #     exit
    # }

}

END{

    print "}"
}
