#!/bin/bash
without_param () {

	echo >&2 "$@"
	exit 1	
}

download_protein(){

	wget https://files.rcsb.org/download/$1.pdb
}

process_atom(){

	#http://www.wwpdb.org/documentation/file-format-content/format33/sect9.html#ATOM

	#Divido las columnas que solo me sirven segun referencia
	
	
	#se divide el archivo.pdb en 2 archivos de interés, uno que sólo contiene las líneas "ATOM" 
	#y uno que sólo contiene las líneas "HETATM" excluyendo las que pertenecen a moléculas de HOH:
	cat $1.pdb |grep "^ATOM" |awk ' {print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7 " " $8 " " $9}' > $1_ATOM.txt
	cat $1.pdb |grep "^HETATM" |awk '($4 != HOH) {print $1 " " $2 " " $3 " " $4 " " $5 " " $6 " " $7 " " $8 " " $9}' > $1_HETATM.txt	

	

	#remuevo espacios en blancos

	sed 's/ //g' temp > temp1

	mv temp1 temp
}


process_hetatm(){
	#se define la funcion "process_hetatm", la cual llama a al archivo process_HETATM.awk para generar un archivo de texto
	#cuyas líneas consisten en los centros geométricos de cada ligando por sí solo

    process_HETATM.awk $1 > resultados.txt
    
}

calculador_rango(){
	
	#la idea de esta funcion es que tome el archivo resultados.txt de la función anterior,
	#el archivo $P1_ATOM.txt, que corresponde a los ATOM de la proteina descargada, y compare las columnas $7, $8 y $9
	#de este último para ver que sean <= que 
	#CentroX + RANGO, CentroY + RANGO y CentroZ + RANGO del archivo resultados.txt ($1, $2 y $3 respectivamente; 
	#RANGO corresponde al rango entregado por el usuario)
	#las lineas ATOM que cumplan con la condición se irían a un archivo temp junto con el centro geométrico del ligando 
	#al que corresponden.
	
	
	limitador.awk $1 $2 $3
	
}


gen_graph(){
	
	#el archivo temp pasaría luego a esta función, de la cual aún falta modificar el "puntodoteador.awk",
	#el cual corresponde al archivo.awk de nombre gen_graph.awk entregado por el profesor.

	rm -f graph.dot graph.png

	awk -F';' -f ../puntodoteador.awk temp >> graph.dot

	dot -Tsvg graph.dot -o graph.svg

	epiphany graph.svg
}

if [[ ! -d "proteínas" ]]; then

	mkdir proteínas
	cd proteínas
fi



[ "$#" -eq 1 ] || without_param "Un parámetro es requerido, $# es entregado"

#Parametro siempre en mayúscula

P1=$(echo $1 | tr a-z A-Z)

#obtengo la linea, cuidado con las cooincidencias.

#Para evitar multiples resultados se realiza un escape para comillas

PROTEIN=$(cat bd-pdb.txt |grep \"$P1\")

#filtro resultado y reemplazo "," por ; (punto y coma)

PROTEIN=$(echo $PROTEIN |sed -e 's/\","/;/g')

#Utilizo AWK para obtener la columna

PROTEIN=$(echo $PROTEIN |awk -F';' '{print substr ($4, 1, length($4)-1)}')

#convierto a mayuscula el resultado

PROTEIN=$(echo $PROTEIN |tr a-z A-Z)

echo $PROTEIN

#se pide el rango en que el usuario desea graficar los elementos relacionados a los ligandos.
echo "Ingrese el rango en angstroms en que quiere graficar los elementos."
read RANGO


if [[ $PROTEIN == "PROTEIN" ]]; then

	mkdir $P1 2>/dev/null

	cd $P1

	download_protein $P1

	process_atom $P1
	
	#se llama a la función process_hetatm con el archivo HETATM de la proteína descargada como parámetro para generar
	#el archivo resultados.txt que corresponde a los centros geométricos de cada ligando.
	process_hetatm $P1_HETATM.txt
	
	#se llama a la función calculador_rango con los parámetros correspondientes (2 archivos que se deben comparar y
	#el rango entregado por el usuario.
	calculador_rango resultados.txt $P1_ATOM.txt RANGO

	puntodoteador


else

	echo "No es proteina"

fi
